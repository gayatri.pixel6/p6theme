<?php
   /*
   Template Name: Two-Columns-page
   */
   get_header();
   $show_service = get_post_meta(get_the_ID(), 'show_service', true);
   $display_project_section = get_post_meta($post->ID, 'display_project_section', true);
   $show_banner = get_post_meta(get_the_ID(), 'show_banner', true);
   $repeater_fields = get_post_meta($post->ID, 'repeater_fields', true);
   $textArray = !empty($repeater_fields) ? json_encode($repeater_fields) : json_encode(["Hello", "World", "Typewriter"]);
   
   
   
   ?>
<style>
   /* .client_two_col {
   padding-top: 66px !important;
   padding-bottom: 66px;
   } */
</style>
<div class="row" style="border-bottom: 1px solid #e2e1e1;">
   <div class="col-md-4">
      <?php get_sidebar(); ?>
   </div>
   <div class="col-md-8" style="padding-left: 70px;">
      <div class="" style="padding-top: 42px;">
      <?php if ($show_banner === 'on') : ?>
    <div class="px_project_heading" id="about">
        <?php while (have_posts()) : the_post(); ?>
            <?php 
                $page_title = get_post_meta(get_the_ID(), 'page_title', true);
                $page_subtitle = get_post_meta(get_the_ID(), 'page_subtitle', true);
            ?>
            <?php if (is_front_page()) : ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="clearfix px_header_bottom two-coloumn-banner" id="bannertext">
                            <h1 class="page-title"><span class="textchange"></span></h1>
                            <h3><?php echo esc_html($page_subtitle); ?></h3>
                        </div>
                    </div>
                </div>
            <?php elseif (empty($page_title)) : ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="clearfix px_header_bottom two-coloumn-banner">
                            <h1 class="page-title"><?php echo esc_html(get_the_title()); ?></h1>
                            <?php if ($page_subtitle) : ?>
                                <h3><?php echo esc_html($page_subtitle); ?></h3>
                            <?php else : ?>
                                <h3>page subtitle</h3>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php else : ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="clearfix px_header_bottom two-coloumn-banner" id="bannertext">
                            <h1 class="page-title"><?php echo esc_html($page_title); ?></h1>
                            <h3><?php echo esc_html($page_subtitle); ?></h3>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
    </div>
<?php else : ?>
    <h1><?php echo esc_html(get_the_title()); ?></h1>
<?php endif; ?>

      <!-- for get the content part it could be any description or contact form -->
      <!-- Two-Column Layout HTML -->
      <?php if(get_the_content() != '') { ?>
      <div class="row">
         <div class="col-12 col-md-6 form-container ">
            <p><?php echo do_shortcode(get_the_content()); ?></p>
         </div>
      </div>
      <?php }?>
      <!-- <div class="px_content px_header_bottom">
         <p><?php //echo do_shortcode(get_the_content()); ?></p>
         </div> -->
      <section class="clearfix  slideshow_wrap">
         <?php
            $show_client = get_post_meta(get_the_ID(), 'show_client', true);
            
            if ($show_client === 'on') { ?>
         <div class="clients-wrapper clearfix client_two_col">
            <h2>Our Clients</h2>
            <div class="client-block">
               <ul class="client-logos clearfix">
                  <?php
                     $args = array(
                         'post_type'      => 'client',
                         'posts_per_page' => 5,
                         'order'          => 'ASC',
                     );
                     $query = new WP_Query($args);
                     if ($query->have_posts()) :
                         while ($query->have_posts()) : $query->the_post();
                             ?>
                  <li class="company-logo">
                     <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'medium'); ?>" alt="logo">
                  </li>
                  <?php
                     endwhile;
                     wp_reset_postdata();
                     else :
                     echo '<p>No clients found.</p>';
                     endif;
                     ?>
               </ul>
            </div>
         </div>
         <?php } ?>
      </section>
   </div>
</div>
<?php
   $show_project = get_post_meta(get_the_ID(), 'show_project', true);
   if ($show_project === 'on') { ?>
<div id="primary" class="content-area">
   <main id="main" class="site-main">
      <section class="clearfix banner-section">
         <div class="px_container_top clearfix">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
               <div class="carousel-inner">
                  <?php
                     $args = array(
                         'post_type' => 'portfolio',
                         'orderby' => 'rand',
                         'posts_per_page' => 3, // Display three slides
                         'portfolio_category' => 'slideshow'
                     );
                     $query = new WP_Query($args);
                     $first_slide = true;
                     $slide_index = 0;
                     if ($query->have_posts()) :
                         while ($query->have_posts()) : $query->the_post();
                     ?>
                  <div class="project_details_img">
                     <img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/project_mberry.png" alt="slide1" />
                  </div>
                  <div class="carousel-item <?php if ($first_slide) {
                     echo 'active';
                     $first_slide = false;
                     } ?>">
                     <div class="slides_wrappper">
                        <div class="px_slideshow">
                           <div class="slides slide_1 slideanimate">
                              <div class="px_slideshow_left" style="padding-right:25px;">
                                 <div class="px_post_heading">
                                    <h3 class="slide_title"><?php the_title(); ?></h3>
                                    <span class="slide_subtitle"></span>
                                 </div>
                                 <div class="px_post_content">
                                    <div class="post_content_slider">
                                       <?php echo get_the_excerpt(); ?>
                                    </div>
                                 </div>
                                 <?php
                                    $btn_link_text = get_post_meta(get_the_ID(), 'btn_link_text', true);
                                    $btn_link = get_post_meta(get_the_ID(), 'btn_link', true);
                                    if ($btn_link_text && $btn_link) :
                                    ?>
                                 <a href="<?php echo esc_url($btn_link); ?>" class="white_button view_button clearfix" target="_blank"><?php echo esc_html($btn_link_text); ?><span>&raquo;</span></a>
                                 <?php endif; ?>
                              </div>
                              <div class="px_slideshow_right">
                                 <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?>" alt="logo">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php
                     $slide_index++;
                     endwhile;
                     wp_reset_postdata();
                     endif;
                     ?>
               </div>
               <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
               <span class="carousel-control-prev-icon" aria-hidden="true"></span>
               <span class="sr-only">Previous</span>
               </a>
               <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
               <span class="carousel-control-next-icon" aria-hidden="true"></span>
               <span class="sr-only">Next</span>
               </a>
            </div>
            <!-- Circle Buttons for Carousel Control -->
            <div class="carousel-control-buttons">
               <?php for ($i = 0; $i < $slide_index; $i++) : ?>
               <button type="button" data-target="#carouselExampleControls" data-slide-to="<?php echo $i; ?>" class="<?php if ($i === 0) echo 'active'; ?>"></button>
               <?php endfor; ?>
            </div>
         </div>
      </section>
   </main>
</div>
<?php } ?>
<script>
   document.addEventListener('DOMContentLoaded', function () {
       var buttons = document.querySelectorAll('.carousel-control-buttons button');
       buttons.forEach(function (button, index) {
           button.addEventListener('click', function () {
               var targetSlideIndex = index;
               var carousel = document.querySelector('#carouselExampleControls .carousel-inner');
               var slides = carousel.querySelectorAll('.carousel-item');
   
               slides.forEach(function(slide, i) {
                   if (i === targetSlideIndex) {
                       slide.style.opacity = 1;
                       slide.classList.add('active');
                   } else {
                       slide.style.opacity = 0;
                       slide.classList.remove('active');
                   }
               });
   
               // Update button active states
               buttons.forEach(function(btn, j) {
                   btn.classList.remove('active');
                   if (j === targetSlideIndex) {
                       btn.classList.add('active');
                   }
               });
           });
       });
   });
</script>
<?php 
   if($display_project_section == 'on') { ?>
<div id="primary" class="content-area">
   <main id="main" class="site-main">
      <section class="clearfix banner-section">
         <div class="px_container_top clearfix">
            <?php
               $args = array(
                  'post_type' => 'project',
                  'orderby' => 'rand',
                  'posts_per_page' => 1,
                  'project_category' => 'slideshow'
               );
               $query = new WP_Query($args);
               if ($query->have_posts()) :
                  while ($query->have_posts()) : $query->the_post();
               ?>
            <div class="slides_wrappper">
               <div class="project_details_img">
                  <img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/img/project_mberry.png" alt="slide1" />
               </div>
               <div class="px_slideshow">
                  <div class="slides slide_1 slideanimate">
                     <div class="px_slideshow_left" style="padding-right:25px;">
                        <div class="px_post_heading">
                           <h3 class="slide_title"><?php the_title(); ?></h3>
                           <span class="slide_subtitle"></span>
                        </div>
                        <div class="px_post_content">
                           <div class="post_content_slider">
                              <?php echo get_the_excerpt(); ?>
                           </div>
                        </div>
                        <?php
                           $btn_link_text = get_post_meta(get_the_ID(), 'btn_link_text', true);
                           $btn_link = get_post_meta(get_the_ID(), 'btn_link', true);
                           if ($btn_link_text && $btn_link) :
                           ?>
                        <a href="<?php echo esc_url($btn_link); ?>" class="white_button view_button clearfix" target="_blank"><?php echo esc_html($btn_link_text); ?><span>&raquo;</span></a>
                        <?php endif; ?>
                     </div>
                     <div class="px_slideshow_right px_slideshow_right_front">
                        <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'medium'); ?>" alt="logo">
                     </div>
                  </div>
               </div>
            </div>
            <?php
               endwhile;
               wp_reset_postdata();
               endif;
               ?>
         </div>
      </section>
   </main>
</div>
<?php } ?>
</div>
</div>
<?php
   $args = array(
       'post_type'      => 'service',
       'posts_per_page' => 3,
   );
   $query = new WP_Query($args);
   if ($query->have_posts()) :
   
   if( $show_service == 'on') { ?>
<div class="row">
   <div class="px_container_bottom">
      <div class="px_services_list content_wrapper_width clearfix">
         <?php
            while ($query->have_posts()) : $query->the_post();
                $service_subtitle = get_post_meta(get_the_ID(), 'service_subtitle', true);
            ?>
         <article class="px_services px_3coloumn">
            <div>
               <h2><?php echo get_the_title(); ?></h2>
               <span><?php echo esc_html($service_subtitle); ?></span>
            </div>
            <p class="service_details"><?php echo get_the_excerpt(); ?></p>
         </article>
         <?php
            endwhile;
            wp_reset_postdata();
            ?>
      </div>
   </div>
</div>
<?php } ?>
<?php endif; ?>
<?php  get_footer(); ?>
<script>
   var textArray = <?php echo json_encode($repeater_fields); ?>;  // PHP variable to JavaScript
   
   // Provide default values if textArray is empty or contains only empty strings
   if (!textArray || textArray.length === 0 || textArray.every(function(value) { return value === ""; })) {
       textArray = ['Banner Title 1', 'Banner Title 2'];
   }
   
   var currentWordIndex = 0;  // Current word index
   var currentCharIndex = 0;   // Current character index
   var isTyping = true;        // Flag to track typing or backspacing mode
   
   function typeWriter(id, ar) {
       var element = jQuery("#" + id),
           eHeader = element.find("span.textchange");
   
       // Ensure eHeader is found
       if (eHeader.length === 0) {
           console.error("No <span> element found within the specified element.");
           return;
       }
   
       var currentWord = ar[currentWordIndex];
   
       if (isTyping) {
           // Typing mode
           eHeader.text(currentWord.substring(0, currentCharIndex++));
   
           // Check if typing is complete for the current word
           if (currentCharIndex <= currentWord.length) {
               setTimeout(function() { typeWriter(id, ar); }, 200);  // Typing speed (slower)
           } else {
               isTyping = false;  // Switch to backspacing mode
               setTimeout(function() { typeWriter(id, ar); }, 1000);  // Delay before starting backspacing
           }
       } else {
           // Backspacing mode
           eHeader.text(currentWord.substring(0, --currentCharIndex));
   
           // Check if backspacing is complete for the current word
           if (currentCharIndex === 0) {
               isTyping = true;  // Switch to typing mode for the next word
               currentWordIndex = (currentWordIndex + 1) % ar.length;  // Move to the next word
               setTimeout(function() { typeWriter(id, ar); }, 1000);  // Delay before typing next word (slower)
           } else {
               setTimeout(function() { typeWriter(id, ar); }, 50);  // Backspacing speed (slower)
           }
       }
   }
   
   // Start the typewriter effect when the document is ready
   jQuery(document).ready(function($) {
       typeWriter("bannertext", textArray);
   });
</script>
<script>
   jQuery(document).ready(function($) {
    // Find all <p> tags within .wpcf7-form that contain checkboxes
    $('.wpcf7-form span').each(function() {
        if ($(this).find('input[type="checkbox"]').length > 0) {
            $(this).addClass('has-checkbox');
        }
    });
   });
   
   
   jQuery(document).ready(function($) {
    // Find all <p> tags within .wpcf7-form that contain checkboxes
    $('.wpcf7-form span').each(function() {
        if ($(this).find('input[type="radio"]').length > 0) {
            $(this).addClass('has-radio');
        }
    });
   });
</script>