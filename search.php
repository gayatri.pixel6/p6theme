<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package p6-Theme
 */

get_header();
?>

<main id="primary" class="site-main container">
    <div class="row">
        <div class="col-md-12">
            <div class="clearfix px_header_bottom px_title_section content_wrapper_width">
                <h1>Search Results</h1>
                <h3>for: "<?php echo esc_html(get_search_query()); ?>"</h3>
            </div>
        </div>
    </div>

    <div class="row page_not_found">
        <div id="content" class="site-content row">
            <div class="col-md-8">
                <section id="primary" class="content-area">
                    <main id="main" class="site-main">

                        <?php if (have_posts()) : ?>

                            <?php while (have_posts()) : the_post(); ?>
                                <!-- Your loop content here -->
                            <?php endwhile; ?>

                        <?php else : ?>

                            <section class="no-results not-found">
                                <header class="page-header">
                                    <h1 class="page-title">Nothing Found</h1>
                                </header><!-- .page-header -->
                                <div class="page-content">

                                    <p>Sorry, but nothing matched your search terms. Please try again with some different keywords.</p>
                                    <form role="search" method="get" class="search-form" action="<?php echo esc_url(home_url('/')); ?>">
                                        <label>
                                            <span class="screen-reader-text">Search for:</span>
                                            <input type="search" class="search-field" placeholder="Search …" value="<?php echo esc_attr(get_search_query()); ?>" name="s">
                                        </label>
										<button type="submit" class="search-submit btn btn_submit"><?php echo esc_html( 'Search' ); ?></button>
                                    </form>
                                </div><!-- .page-content -->
                            </section><!-- .no-results -->

                        <?php endif; ?>

                    </main><!-- #main -->
                </section><!-- #primary -->
            </div><!-- .col-md-8 -->

            <div class="col-md-4">
                <div id="secondary" class="widget-area" role="complementary">
                    <div class="coloumn_25">
                        <div class="px_sidebar_details">
                            <div class="px_sidebar_heading">
                                <h3>TOPICS</h3>
                                <span>What we talk about</span>
                            </div>
                            <div class="px_aside_desc">
                                <ul>
                                    <li><a href="#">Pixel6 Blog</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="px_sidebar_details">
                            <div class="px_sidebar_heading">
                                <h3>ARCHIVE</h3>
                                <span>past thoughts</span>
                            </div>
                            <div class="px_aside_desc">
                                <ul>
                                    <!-- Archive items -->
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="coloumn_25 last_coloumn_25">
    <div class="project_details">
        <div class="px_sidebar_heading">
            <h3>Tags</h3>
            <span>favourite topics</span>
        </div>
        <div class="px_aside_desc">
            <ul>
                <?php
                // Get tags associated with the current post
                $tags = get_tags();

                if ($tags) {
                    foreach ($tags as $tag) {
                        // Output each tag as a list item with a link
                        echo '<li><a href="' . esc_url(get_tag_link($tag->term_id)) . '" title="' . esc_attr(sprintf(__('View all posts tagged %s'), $tag->name)) . '">' . esc_html($tag->name) . '</a></li>';
                    }
                }
                ?>
            </ul>
        </div>
    </div>
</div>

                </div><!-- #secondary -->
            </div><!-- .col-md-4 -->

        </div><!-- .row -->
    </div><!-- .page_not_found -->

</main><!-- #main -->

<?php
get_footer();
?>
