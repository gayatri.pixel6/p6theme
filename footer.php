<?php
/**
* The template for displaying the footer.
*
* Contains the closing of the #content div and all content after
*
* @package pixel
*/
?>
</div>
<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package pixel
 */
?>

<footer class="px_footer clearfix">
    <div class="px_footer_wrapper content_wrapper_width">
        <div class="px_view clearfix">
            <div class="px_3coloumn">
                <div class="px_view_section">
                    <?php if (is_active_sidebar('pixel6-jobs-widget-area')) : ?>
                        <?php dynamic_sidebar('pixel6-jobs-widget-area'); ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="px_3coloumn">
                <div class="px_view_section">
                    <?php if (is_active_sidebar('pixel6-social-widget-area')) : ?>
                        <?php dynamic_sidebar('pixel6-social-widget-area'); ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="px_3coloumn">
                <div class="px_view_section px_contact_section">
                    <?php if (is_active_sidebar('pixel6-gmail-widget-area')) : ?>
                        <?php dynamic_sidebar('pixel6-gmail-widget-area'); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="px_footer_bottom custom_footer clearfix navbar-nav">
        <nav class="navbar navbar-expand-lg navbar-light px_footer_nav ">
               <div class="collapse navbar-collapse footer_nav" id="navbarTogglerDemo03">
							 <?php 
                        wp_nav_menu(array(
                           'theme_location' => 'footer-menu',
                           'menu' => 'navbarTogglerDemo03',
                           'menu_class' => 'navbar-nav mr-auto mt-2 mt-lg-0',
                           'container' => false, // Remove the default container
                         
                        ));
                        ?>
                  </div>
                  <div class="copyright_section ">
							<span>© <?php the_date('Y'); ?>  Pixel6 Web Studio Pvt. Ltd.</span>
						</div>
					</div>
               </nav>

						
    </div>
</footer><!-- #colophon -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
