<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package p6-Theme
 */

get_header();
?>

<main id="primary" class="site-main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="px_header_bottom px_title_section content_wrapper_width">
                    <h1>404!</h1>
                    <h3>Oops! Looks like this page does not exist or is under construction. We apologize!</h3>
                </div>
            </div>
            <!-- .col-md-12 -->
            <div class="col-md-12">
                <div class="row page_not_found">
                    <div class="col-md-8">
                        <section class="error-404 not-found">
                            <header class="page-header">
                                <h1 class="page-title">Oops! That page can’t be found.</h1>
                            </header>
                            <!-- .page-header -->
                            <div class="page-content">
                                <p>It looks like nothing was found at this location.</p>
                                <form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                                    <label>
                                        <span class="screen-reader-text">Search for:</span>
                                        <input type="search" class="search-field form-control p-1" placeholder="Search …" value="<?php echo get_search_query(); ?>" name="s">
                                    </label>
                                    <button type="submit" class="search-submit btn btn_submit"><?php echo esc_html( 'Search' ); ?></button>
                                </form>
                            </div>
                            <!-- .page-content -->
                        </section>
                        <!-- .error-404 -->
                    </div>
                    <!-- .col-md-8 -->
                    <div class="col-md-4">
                        <div class="px_sidebar_details">
                            <div class="px_sidebar_heading">
                                <h3>SITE MAP</h3>
                                <span>Menu</span>
                            </div>
							<div class="px_aside_desc">
    <ul>
        <?php
        // Define the menu location you want to retrieve
        $menu_location = 'top-menu';

        // Get the menu object based on location
        $menu_object = wp_get_nav_menu_object( $menu_location );

        // If menu exists, fetch and display menu items
        if ( $menu_object ) {
            $menu_items = wp_get_nav_menu_items( $menu_object->term_id );

            // Loop through each menu item
            foreach ( $menu_items as $menu_item ) {
                $url = $menu_item->url;
                $title = $menu_item->title;
                echo '<li><a href="' . esc_url( $url ) . '">' . esc_html( $title ) . '</a></li>';
            }
        }
        ?>
    </ul>
</div>

                            <!-- .px_aside_desc -->
                        </div>
                        <!-- .px_sidebar_details -->
                    </div>
                    <!-- .col-md-4 -->
                </div>
                <!-- .row -->
            </div>
            <!-- .col-md-12 -->
        </div>
        <!-- .row -->
    </div>
    <!-- .container -->
</main>
<!-- #primary -->

<?php
get_footer();
?>
