<div id="primary" class="content-area">
   <main id="main" class="site-main">
      <section class="clearfix px_container banner-section">
         <div class="px_container_top content_wrapper_width clearfix">
            <div class="row">
               <div class="col-md-4 col-lg-4">
                  <div class="">
                     <div class="px_project_heading">
                        <h2>Jobs At Pixel6</h2>
                        <!-- 						<span>get in touch with us</span> -->
                     </div>
                     <div class="px_project_desc clearfix">
                        <p>We are always looking for passionate and eager-to-learn recent Engineering/MCA graduates. If you have good web development skills and are interested in a career at Pixel6, please fill out and submit the following form.
                        </p>
                        <h3>The job</h3>
                        <p>
                           Software Trainee Engineer<br>
                           Education: BE/BTECH/MCA/MCS<br>
                           Location: Baner, Pune<br>
                           Skills: Javascript/PHP/Angular/Java 
                        </p>
                        <p>
                           Software Trainee Engineer<br>
                           Education: BE/BTECH/MCA/MCS<br>
                           Location: Baner, Pune<br>
                           Skills: Javascript/PHP/Angular/Java 
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-8 col-lg-8">
                  <div class="">
                     <div class="coloum_btn_wrapper coloumn_last-contact ">
                        <a href="#career"  class="white_button" id="career">Applying for Software Trainee Engineer</a>
                     </div>
                     <div class="px_service_quote px_whitebg_quote" >
                        <div class="px_cont_form">
                           <div class="px-form" id="form_career">
                              <?php echo do_shortcode(get_the_content());?> 								
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </main>
   <!-- #main -->
</div>
<?php get_footer();?>