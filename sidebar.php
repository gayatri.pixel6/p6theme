<?php
$heading = get_post_meta($post->ID, 'heading', true);
$content = get_post_meta($post->ID, 'heading_para_content', true);

// Set default values if meta values are empty
$default_heading = 'Sidebar Section';
$default_content = 'Default paragraph content. Edit me!';

if (empty($heading)) {
    $heading = $default_heading;
}

if (empty($content)) {
    $content = $default_content;
}
?>

<style>
/* Additional styles */
.auther_details p:first-of-type {
    margin-bottom: -65px;
}
</style>

<div class="d-flex flex-column flex-shrink-0 p-3" style="width: 280px;">

    <section class="clearfix profile">

        <div class="px_container_top content_wrapper_width clearfix">

            <h1 class="heading_per_page"> <?php echo esc_html($heading); ?></h1>
            <p><?php echo wp_kses_post($content); ?></p>


            <div class="px_1coloumn coloumn_first left-column">

                <div class="testimonial_sec clearfix">

                    <?php
                    $show_testimonials = get_post_meta(get_the_ID(), 'show_testimonials', true);

                    if ($show_testimonials === 'on') { ?>
                        <div class="clearfix services_quote_wrapper">
                            <?php
                            $args = array(
                                'post_type'      => 'testimonial',
                                'posts_per_page' => 1,
                                'orderby'        => 'rand',
                                'order'          => 'ASC',
                            );
                            $posts_array = get_posts($args);
                            foreach ($posts_array as $quote) {
                                ?>
                                <div class="px_3coloumn">
                                    <blockquote class="px_service_quote px_whitebg_quote">
                                        <div class="px_author_sec">
                                            <div class="quote-author-img">
                                                <?php if (has_post_thumbnail($quote->ID)) : ?>
                                                    <img src="<?php echo esc_url(get_the_post_thumbnail_url($quote->ID, 'medium')); ?>" style="width: 40px;" alt="Author Image">
                                                <?php else : ?>
                                                    <img src="<?php echo esc_url(get_stylesheet_directory_uri() . '/img/user-placeholder.png'); ?>" style="width: 40px;" alt="Placeholder Image">
                                                <?php endif; ?>
                                            </div>
                                            <div class="auther_details aa">
                                                <p><?php echo esc_html($quote->post_title); ?></p>
                                                <p><small><?php echo esc_html($quote->post_content); ?></small></p>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
</div>
