<?php get_header(); ?>

<div class="row">
    <div class="col-md-12">
        <div class="clearfix px_header_bottom px_title_section content_wrapper_width" id="bannertext">
            <h1 class="page-title"><?php echo get_the_title(); ?></h1>
            <h3><?php echo get_the_author(); ?> - <?php echo get_the_date(); ?></h3>
        </div>
    </div>
</div>

<div id="content" class="site-content row">
    <div class="col-md-8">
        <div class="px_container_top single_container_top content_wrapper_width clearfix">
            <div class="px_2combinecoloumn">
                <div id="primary" class="content-area">
                    <main id="main" class="site-main">
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <div class="entry-content px_service_quote px_whitebg_quote">
                                <p><?php echo get_the_excerpt(); ?></p>
                                <figure class="wp-block-image size-full is-style-rounded">
                                    <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?>" alt="logo" class="wp-image-325">
                                </figure>
                            </div>
                        </article>
                    </main>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-md-4 px_tag">
        <div class="coloumn_25 last_coloumn_25">
            <div class="project_details">
                <div class="px_tag_details">
                    <h2 class="widget-title"><?php _e('Tags'); ?></h2>
                    <ul class="tag-list">
                        <?php 
                        $tags = get_tags(array(
                            'hide_empty' => false
                        ));
                        if ($tags) {
                            foreach ($tags as $tag) {
                                echo '<li class="tag-item"><a href="' . get_tag_link($tag->term_id) . '">' . esc_html($tag->name) . '</a></li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php get_footer(); ?>
