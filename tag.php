<?php get_header(); ?>

<?php
// Get the current tag object
$current_tag = get_queried_object();
?>

<h1><?php echo single_tag_title('', false); ?></h1>

<?php if ($current_tag && !is_wp_error($current_tag)) : ?>
    <p><?php echo esc_html($current_tag->description); ?></p>
<?php endif; ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <p><?php the_content(); ?></p>
<?php endwhile; endif; ?>
</div>
<?php get_footer(); ?>