<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>P6</title>
      <!-- Bootstrap CSS -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
      <?php wp_head(); ?>
   </head>
   <body>
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-sm-12">
               <nav class="navbar navbar-expand-lg navbar-light px_header">
                      <div class="logo">
                        <?php 
                        if (has_custom_logo()) {
                           the_custom_logo();
                        } else {
                           // Display default logo image here
                           ?>
                           <img src="<?php echo get_template_directory_uri() . '/img/logo.png'; ?>" alt="Default Logo" class="logo_img">
                           <?php
                        }
                        ?>
                     </div>

                  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
								<?php $walker = new Menu_With_Description; ?>

                     <?php 
                        wp_nav_menu(array(
                           'theme_location' => 'top-menu',
                           'menu' => 'navbarTogglerDemo03',
                           'menu_class' => 'navbar-nav ms-auto mt-2 mt-lg-0',
                           'container' => false, // Remove the default container
                           // 'walker' => new WP_Bootstrap_Navwalker(),
                           'walker' => $walker,
                        ));
                        ?>
                  </div>
               </nav>
            </div>
         </div>
      
      <?php wp_footer(); ?>
    
      <script>
         $(document).ready(function () {
            $('.navbar-toggler').on('click', function () {
               $('.navbar-collapse').toggleClass('show');
            });
         
            // Close the navbar menu when clicking outside
            $(document).on('click', function (e) {
               if (!$(e.target).closest('.navbar').length) {
                  $('.navbar-collapse').removeClass('show');
               }
            });
         });
      </script>